defmodule TodomvcWeb.Component.ListComponentTest do
  use ExUnit.Case, async: true
  use TodomvcWeb.ConnCase
  import Phoenix.LiveViewTest

  alias TodomvcWeb.Components.ListComponent

  describe("ListComponent should") do
    test "render closed state" do
      view = render_component(ListComponent, [])
      {:ok, doc} = Floki.parse_document(view)

      assert Floki.find(doc, "span.caret-up") == [
               {
                 "span",
                 [{"class", "caret-up"}],
                 [
                   {
                     "img",
                     [{"src", "../images/up-arrow.svg"}, {"height", "20"}, {"width", "20"}],
                     []
                   }
                 ]
               }
             ]
    end
  end
end
