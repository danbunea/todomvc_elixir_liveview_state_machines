defmodule TodosmvcWeb.TodosControllerTest do
  use ExUnit.Case
  use TodomvcWeb.StateMachineCase

  alias TodomvcWeb.TodosController, as: C

  describe "the machine should" do
    test "transits correctly invoking the actions if needed" do
      expectations = %{
        started: %{
          MOUNT: {:mount, :list}
        },
        list: %{
          TOGGLE: {:toggle, :list},
          DELETE: {:delete, :list},
          FILTER: {:filter, :list},
          ADD: {:add, :add},
          EDIT: {:edit, :edit}
        },
        add: %{
          SAVE: {:save, :list},
          CANCEL: :list
        },
        edit: %{
          SAVE: {:save, :list},
          CANCEL: :list
        }
      }

      assert_machine(expectations, C.machine())
    end

    test "machine actions" do
      assert %{
               add: {TodomvcWeb.TodosController, :add},
               delete: {TodomvcWeb.TodosController, :delete},
               filter: {TodomvcWeb.TodosController, :filter},
               mount: {TodomvcWeb.TodosController, :mount},
               toggle: {TodomvcWeb.TodosController, :toggle},
               save: {TodomvcWeb.TodosController, :save}
             } == C.machine().actions
    end
  end

  describe "the controller actions should" do
    @todo_1 %{
      id: 1,
      text: "todo 1",
      done?: true
    }
    @todo_2 %{
      id: 2,
      text: "todo 2",
      done?: false
    }
    @todos %{
      @todo_1.id => @todo_1,
      @todo_2.id => @todo_2
    }

    test "mount" do
      assert %{list: %{todos: %{}, filter: nil}} == C.mount(%{}, %{})
    end

    test "add" do
      assert %{add: %{text: ""}} == C.add(%{}, %{})
    end

    test "save a new one" do
      assert %{
               list: %{
                 todos: %{
                   1 => %{
                     id: 1,
                     text: "add",
                     done?: false
                   }
                 }
               }
             } == C.save(%{list: %{todos: %{}}}, %{text: "add"})

      assert %{
               list: %{
                 todos: %{
                   1 => %{
                     id: 1,
                     text: "exiting",
                     done?: false
                   },
                   2 => %{
                     id: 2,
                     text: "add",
                     done?: false
                   }
                 }
               }
             } ==
               C.save(
                 %{
                   list: %{
                     todos: %{
                       1 => %{
                         id: 1,
                         text: "exiting",
                         done?: false
                       }
                     }
                   }
                 },
                 %{text: "add"}
               )
    end

    test "save a existing one - update" do
      assert %{
               list: %{
                 todos: %{
                   1 => %{
                     id: 1,
                     text: "updated",
                     done?: true
                   }
                 }
               }
             } ==
               C.save(
                 %{
                   list: %{
                     todos: %{
                       1 => %{
                         id: 1,
                         text: "exiting",
                         done?: false
                       }
                     }
                   }
                 },
                 %{id: 1, text: "updated", done?: true}
               )
    end

    test "delete" do
      assert %{list: %{todos: %{}}} ==
               C.delete(
                 %{
                   list: %{
                     todos: %{
                       1 => %{
                         id: 1,
                         text: "exiting",
                         done?: false
                       }
                     }
                   }
                 },
                 %{id: 1}
               )
    end

    test "filter" do
      assert %{list: %{todos: @todos, filter: nil}} ==
               C.filter(%{list: %{todos: @todos}}, %{filter: nil})

      assert %{list: %{todos: @todos, filter: true}} ==
               C.filter(%{list: %{todos: @todos}}, %{filter: true})

      assert %{list: %{todos: @todos, filter: false}} ==
               C.filter(%{list: %{todos: @todos}}, %{filter: false})
    end

    test "toggle" do
      assert %{list: %{todos: %{1 => %{done?: false}}}} ==
               C.toggle(%{list: %{todos: %{1 => %{done?: true}}}}, %{id: @todo_1.id})

      assert %{list: %{todos: %{1 => %{done?: true}}}} ==
               C.toggle(%{list: %{todos: %{1 => %{done?: false}}}}, %{id: @todo_1.id})
    end
  end
end
