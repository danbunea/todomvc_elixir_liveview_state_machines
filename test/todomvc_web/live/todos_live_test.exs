defmodule TodomvcWeb.PageLiveTest do
  use TodomvcWeb.ConnCase

  import Phoenix.LiveViewTest

  test "disconnected and connected render", %{conn: conn} do
    {:ok, page_live, disconnected_html} = live(conn, "/")
    assert disconnected_html =~ "todos"
    assert render(page_live) =~ "todos"
  end
end
