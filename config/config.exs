# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :todomvc,
  todos_controller: TodomvcWeb.TodosController

# Configures the endpoint
config :todomvc, TodomvcWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "WJ3EdGAmy7WMABdtCAweQJZRMESbzU22O6VtQbSLW2hEF2xJuuDw/RGymiVfKwFX",
  render_errors: [view: TodomvcWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Todomvc.PubSub,
  live_view: [signing_salt: "t6fr9kzd"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
