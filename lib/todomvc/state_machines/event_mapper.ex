defmodule TodoMvc.StateMachines.EventMapper do
  @doc """
  Changes String Map to Map of Atoms e.g. %{"c"=> "d", "x" => %{"yy" => "zz"}} to
          %{c: "d", x: %{yy: "zz"}}, i.e changes even the nested maps.
  """
  def atom_map(map), do: to_atom_map(map)

  defp to_atom_map(map) when is_map(map),
    do: Map.new(map, fn {k, v} -> {String.to_atom(k), to_atom_map(v)} end)

  defp to_atom_map(ls) when is_list(ls),
    do: Enum.map(ls, &to_atom_map(&1))

  defp to_atom_map(v), do: v

  def cast_integer(str) when is_binary(str) do
    case Integer.parse(str) do
      {value, ""} -> value
      {_value, _error} -> str
    end
  end

  def cast_integer(non_str) do
    non_str
  end

  def cast_nilable_integer(""), do: nil

  def cast_nilable_integer(str) when is_binary(str) do
    case Integer.parse(str) do
      :error -> nil
      {value, ""} -> value
      {_, _} -> nil
    end
  end

  def update_values(map, list, func) do
    Map.new(
      map,
      fn {k, v} ->
        {
          k,
          if Enum.member?(list, k) do
            func.(v)
          else
            v
          end
        }
      end
    )
  end
end
