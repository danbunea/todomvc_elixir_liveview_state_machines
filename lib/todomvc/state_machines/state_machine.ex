defmodule TodoMvc.StateMachines.StateMachine do
  def transition(machine, from, event) do
    %{machine: machine}
    |> initialize_state(from)
    |> initialize_context(from)
    |> initialize_event(event)
    |> validate_state
    |> validate_event_exists
    |> validate_event_target_exists
    |> apply_actions
    |> apply_transition
    |> collect_result
  end

  def initialize_state(value, from) when is_atom(from) do
    {:ok, put_in(value, [:machine, :initial], from)}
  end

  def initialize_state(value, from) when is_map(from) do
    if Map.has_key?(from, :state) do
      {:ok, put_in(value, [:machine, :initial], from.state)}
    else
      {
        :error,
        %{message: "Missing :state key in from map", value: value, function: :initialize_state}
      }
    end
  end

  def initialize_context({:ok, value}, from) when is_atom(from) do
    {:ok, put_in(value, [:machine, :context], %{})}
  end

  def initialize_context({:ok, value}, from) when is_map(from) do
    {
      :ok,
      if Map.has_key?(from, :context) do
        put_in(value, [:machine, :context], from.context)
      else
        put_in(value, [:machine, :context], %{})
      end
    }
  end

  def initialize_context({:error, _} = error, _), do: error
  def initialize_event({:error, _} = error, _), do: error

  def initialize_event({:ok, value}, {event_name, event_params}) when is_map(event_params) do
    {:ok, put_in(value, [:event], Map.put(event_params, :name, event_name))}
  end

  def initialize_event({:ok, value}, event) when is_atom(event) do
    {:ok, put_in(value, [:event], %{name: event})}
  end

  def validate_state({:error, _} = error), do: error

  def validate_state({:ok, value}) do
    if Map.has_key?(value.machine.states, value.machine.initial) do
      {:ok, value}
    else
      {
        :error,
        %{
          function: :validate_state,
          message: "State not found :#{value.machine.initial}",
          value: value
        }
      }
    end
  end

  def validate_event_exists({:error, _} = error), do: error

  def validate_event_exists({:ok, value}) do
    if get_in(value, [:machine, :states, value.machine.initial, :on, value.event.name]) == nil do
      {
        :error,
        %{
          function: :validate_event_exists,
          message: "For state :#{value.machine.initial} event not found :#{value.event.name}",
          value: value
        }
      }
    else
      existing_event =
        get_in(value, [:machine, :states, value.machine.initial, :on, value.event.name])

      if is_map(existing_event) do
        {:ok, put_in(value, [:event], Map.merge(value.event, existing_event))}
      else
        {:ok, put_in(value, [:event, :target], existing_event)}
      end
    end
  end

  def validate_event_target_exists({:error, _} = error), do: error

  def validate_event_target_exists({:ok, value}) do
    if get_in(value, [:machine, :states, value.event.target]) == nil do
      {
        :error,
        %{
          function: :validate_event_exists,
          message:
            "In #{value.machine.id} for state :#{value.machine.initial} on event :#{
              value.event.name
            } target :#{value.event.target} not found",
          value: value
        }
      }
    else
      {:ok, value}
    end
  end

  def apply_transition({:error, _} = error), do: error

  def apply_transition({:ok, value}) do
    {
      :ok,
      value
      |> put_in([:machine, :value], value.event.target)
      |> Map.get(:machine)
    }
  end

  def apply_actions({:error, _} = error), do: error

  def apply_actions({:ok, value}) do
    if Map.has_key?(value.event, :actions) &&
         Map.has_key?(value.machine.actions, Map.get(value.event, :actions)) do
      action_result =
        case Map.get(value.machine.actions, value.event.actions) do
          {module, func} ->
            apply(module, func, [value.machine.context, value.event])

          anonymous_fn when is_function(anonymous_fn) ->
            anonymous_fn.(value.machine.context, value.event)
        end

      case action_result do
        {:error, _} = error -> error
        {:ok, ctx} -> {:ok, put_in(value, [:machine, :context], ctx)}
        ctx -> {:ok, put_in(value, [:machine, :context], ctx)}
      end
    else
      {:ok, value}
    end
  end

  def collect_result({:error, _} = error), do: error

  def collect_result({:ok, machine}) do
    {:ok, %{state: machine.value, context: machine.context}}
  end
end
