defmodule TodomvcWeb.Base.Controllers.LiveController do
  defmacro __using__([]) do
    quote do
      alias Phoenix.LiveView
      alias TodoMvc.StateMachines.StateMachine
      require Logger

      def send(socket, {event_name, event_params} = event) do
        machine = machine()
        current_state = Map.get(socket.assigns, :state, machine.initial)

        #        Logger.info(
        #          "#{machine.id} in :#{current_state} received :#{event_name} with #{
        #            Jason.encode!(event_params)
        #          }"
        #        )

        StateMachine.transition(
          machine(),
          %{state: current_state, context: Map.delete(socket.assigns, :state)},
          event
        )
        |> collect_state_machine_response(socket, event)
      end

      defp collect_state_machine_response(
             {:error, error_details} = _error,
             socket,
             {event_name, event_params}
           ) do
        machine = machine()
        current_state = Map.get(socket.assigns, :state, machine.initial)

        Logger.warn(
          "WARNING ->  :#{current_state} on :#{event_name} -> with #{Jason.encode!(event_params)} FAILED on function #{
            error_details[:function]
          } and resulted in #{inspect(error_details[:message])} "
        )

        socket
        |> LiveView.assign(:__errors__, error_details)
      end

      defp collect_state_machine_response(
             {:ok, %{state: state, context: context}},
             socket,
             _event
           ) do
        socket
        |> LiveView.assign(
          context
          |> Map.delete(:flash)
        )
        |> LiveView.assign(:state, state)
        |> LiveView.assign(:__errors__, nil)
      end
    end
  end
end
