defmodule TodomvcWeb.TodosController do
  use TodomvcWeb.Base.Controllers.LiveController

  def machine() do
    %{
      id: "Todos machine",
      initial: :started,
      context: %{},
      states: %{
        started: %{
          on: %{
            MOUNT: %{
              actions: :mount,
              target: :list
            }
          }
        },
        list: %{
          on: %{
            TOGGLE: %{
              actions: :toggle,
              target: :list
            },
            DELETE: %{
              actions: :delete,
              target: :list
            },
            FILTER: %{
              actions: :filter,
              target: :list
            },
            ADD: %{
              actions: :add,
              target: :add
            },
            EDIT: %{
              actions: :edit,
              target: :edit
            }
          }
        },
        add: %{
          on: %{
            SAVE: %{
              actions: :save,
              target: :list
            },
            CANCEL: :list
          }
        },
        edit: %{
          on: %{
            SAVE: %{
              actions: :save,
              target: :list
            },
            CANCEL: :list
          }
        }
      },
      actions: %{
        mount: {__MODULE__, :mount},
        toggle: {__MODULE__, :toggle},
        add: {__MODULE__, :add},
        save: {__MODULE__, :save},
        delete: {__MODULE__, :delete},
        filter: {__MODULE__, :filter}
      }
    }
  end

  def mount(ctx, %{}), do: Map.put(ctx, :list, %{todos: %{}, filter: nil})

  def add(ctx, %{}), do: Map.put(ctx, :add, %{text: ""})

  def save(ctx, %{id: id} = updated),
    do: update_in(ctx, [:list, :todos, id], &Map.merge(&1, Map.take(updated, [:text, :done?])))

  def save(ctx, %{text: text}) do
    max =
      ctx.list.todos
      |> Map.values()
      |> Enum.map(& &1.id)
      |> Enum.max(Integer, fn -> 0 end)

    put_in(ctx, [:list, :todos, max + 1], %{
      id: max + 1,
      text: text,
      done?: false
    })
  end

  def toggle(ctx, %{id: id}),
    do: update_in(ctx, [:list, :todos, id, :done?], &(&1 |> Kernel.not()))

  def delete(ctx, %{id: id}), do: update_in(ctx, [:list, :todos], &Map.delete(&1, id))
  def filter(ctx, %{filter: val}), do: put_in(ctx, [:list, :filter], val)
end
