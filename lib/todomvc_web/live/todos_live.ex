defmodule TodomvcWeb.TodosLive do
  use TodomvcWeb, :live_view
  @controller Application.compile_env(:todomvc, :todos_controller)

  def mount(_params, _session, socket) do
    {:ok, assign(socket, query: "", results: %{})}
  end
end
